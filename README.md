# INMO AIR2 SDK 文档
# 总介绍

## 概述
INMO AIR2 SDK是影目为AR内容开发者提供的平台，开发者使用INMO AIR2 SDK进行开发，开发完成后申请上传至INMO应用商店，审核通过后拥有INMO AIR2 AR眼镜的消费者可直接购买下载。
INMO AIR2 SDK提供了基于INMO AIR2 AR眼镜的UI设计，交互适配，INMO Ring2以及基础SLAM AR 6DOF能力的开发套件，支持Unity平台开发和Android平台原生开发，让开发者能快速的集成并开发能运行在INMO AIR2 AR眼镜上的应用。

## 开发规范
### 上架流程
![](/images/commit.PNG) 
#### 获取adb
[如何获取adb调试权限](adb.md)
### 应用包名与签名相关
#### 包名规则
INMO Air2应用使用包名(Package Name)作为应用的唯一标识。
- 包名的命名规则
只能包含大写字母(A到Z)、小写字母(a到z)、数字和下划线，以用点(英文句号)分隔称为断，至少包含2个断，隔开的每一段都必须以字母开头。
- 避免包名冲突
如果尚未发布的应用，包名和其他开发者已经发布的应用重复了，建议立刻修改应用的包名，避免冲突。
- 包名修改
应用发布后，请不要修改包名，一旦您修改了包名，就会被当作一个新的应用，旧版用户也无法收到应用商店的升级提醒。
#### 应用签名相关
为保证应用的更新是由作者本人提交，应用的提交需要签名。
- 所有应用程序必须签名，未经过签名的程序不允许上线。
- 测试和开发阶段，可以使用debug key来为应用签名并提交测试。
- 当应用准备发布时，你需要创建自己的release key来为应用签名，所需要的工具都包含在SDK tools中。
- 你可以使用标准工具——keytool 和 jarsigner/apksigner来生成秘钥和为apk签名。
- 签名前，我们建议你使用zipalign工具来优化apk包。
### 开发者条款
详见[开发者条款](terms.md) 

# 版本更新说明
|  版本   |      时间      |  更新说明 |
| ----------|:-------------:|------:|
| 1.0.0 |  2023.3.30 | 实现6dof空间定位能力的最小版本 |

# Android SDK
## SDK 功能简介
本版块提供了INMO AIR2 SDK Android平台的开发指南，其中介绍了如何在INMO Air2上的INMO RING2的适配开发，INMO Air2 AR眼镜触控板交互开发，使用INMO Air2 AR眼镜的Camera以及传感器等等；以及Android平台的AR能力开发套件
### 快速上手
#### 下载安卓demo
git clone [git@gitee.com:inmolens/inmo-ar-sdk-sample.git]()
#### 环境依赖
- Android Studio。
- JDK 11。

#### SDK AAR包
引入依赖方法：
1. 项目build.gradle中添加仓库依赖（高版本的Android Studio在settings.gradle）：
```java
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
        jcenter()
        maven { url 'https://jitpack.io' }
        ////增加inmosdk仓库依赖
        maven{url 'https://gitee.com/inmolens/inmo-ar-sdk/raw/master'}
    }
}
```
2. app模块的build.gralde中添加inmo sdk远程依赖:
```java
dependencies {
    //INMO AR SDK
    implementation 'com.inmo:inmo_arsdk:0.0.1'
    ......
}
```

#### 系统要求
如需运行影目AR服务，请保证您的INMO Glass Air系统版本在2.4版本以上。


## INMO RING2开发适配
### 简介
本版块介绍如何使运行在INMO Air2 AR眼镜上的App适配INMO Ring2指环的交互
### API参考
在INMO Air2中，当INMO Ring2已与INMO Air2成功连接后，INMO Ring2的按键事件以键值码传至应用层，以Activity为例，重写onKeyDown方法，在方法体内根据不同的keycode处理相应的业务逻辑：
```java
public class MainActivity extends AppCompatActivity {
 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
 
    //响应INMO Ring2按键
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            //在这里根据INMO Ring2 的keycode处理您的业务逻辑
        }
        return super.onKeyDown(keyCode, event);
    }
}
```
INMO Ring2 目前提供超过5种按键功能，让开发者能够构建出丰富的应用交互。关于不同按键的键值表，请参考本版块中的键值表
### 戒指按键键值表
| 按键 |	功能 |	Keycode |	备注 |
|:----------|:-------------:|:-------------:|:-------------|
|单击/双击/长按 “OK”键|	确认|	66|	KEYCODE_ENTER|
|单击/双击/长按 “上”键|	方向上|	19|	KEYCODE_DPAD_UP|
|单击/双击/长按 “下”键|	方向下|	20|	KEYCODE_DPAD_DOWN|
|单击/双击/长按 “左”键|	方向左|	21|	KEYCODE_DPAD_LEFT|
|单击/双击/长按 “右”键|	方向右|	22|	KEYCODE_DPAD_RIGHT|
|单击“返回”键|	退出/返回|	4|	KEYCODE_BACK|
|双击“返回”键|	回到桌面|	3|	KEYCODE_HOME|

## INMO Air2 触控板交互适配
### 简介
本版块介绍如何在INMO Air2 AR眼镜上使用镜腿处的触控板构建开发运行在INMO Air2 AR眼镜上的Android App，参考SDK中的demo程序，可构建在INMO Air2 AR眼镜运行的基于触控交互的App；Demo将会提供一些关于如何构建在INMO Air2  AR眼镜运行并交互的控件使用方法，开发者可参考并构建属于自己的App。（但可能需要开发者对Android事件分发体系有一定了解）。
### API参考
在INMO Air2中，触控板的触摸手势以键值码传至应用层，以Activity为例，重写onKeyDown方法，在方法体内根据不同的keycode处理相应的业务逻辑：

```java
public class MainActivity extends AppCompatActivity {
 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
 
    //响应触控板手势
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            //在这里根据触控板的keycode处理您的业务逻辑
        }
        return super.onKeyDown(keyCode, event);
    }
}
```
INMO Air2 AR眼镜的触控板目前提供超过10种不同手势功能，让开发者能够构建出丰富的应用交互。关于不同手势的键值表，请参考本版块中的键值表

### 触控板键值表
| 手势/键值(右触控板) | Keycode | 备注                 |
|-------------|:---------:|--------------------|
| 单指/双指 单击    | 66      | KEYCODE_ENTER      |
| 单指/双指 双击    | 4       | KEYCODE_BACK       |
| 单指长按        | 289     | /                  |
| 单指前划        | 22      | KEYCODE_DPAD_RIGHT |
| 单指后划        | 21      | KEYCODE_DPAD_LEFT  |
| 单指上划        | 19      | KEYCODE_DPAD_UP    |
| 单指下划        | 20      | KEYCODE_DPAD_DOWN  |
| 双指长按        | 290     | /                  |
| 双指前划        | /       | 连续上报KEYCODE_DPAD_RIGHT               |
| 双指后划        | /       | 连续上报KEYCODE_DPAD_LEFT             |
| 双指上划        | /       | 连续上报KEYCODE_DPAD_UP               |
| 双指下划        | /       | 连续上报KEYCODE_DPAD_DOWN               |

## Camera、传感器等外设
### 摄像头打开
INMO Air2 AR眼镜基于Android系统深度定制，因此访问摄像头等硬件完美兼容Android开发的SDK；INMO Air2上推荐的获取摄像头对象并打开的方式如下：
```java
private Camera camera;
private int INMO_AIR2_RGB_CAMERA_ID = 0;//普通摄像头
camera = Camera.open(INMO_AIR2_RGB_CAMERA_ID);//打开普通摄像头
```
## 传感器等其他外设开发
INMO Air2 AR眼镜基于Android系统深度定制，因此兼容安卓平台的通用接口。访问外部设备和传感器，可参考安卓官方参考文档：https://developer.android.google.cn/docs


## 原生Android AR开发
### 简介
本版块介绍了如何使用INMO AR服务构建可运行在INMO Air2 AR眼镜上的应用，INMO AR服务是一套用于在Inmo AR眼镜上构建增强现实应用的算法和服务集合。目前算法支持6dof空间定位能力，由于算力和电池容量的限制，暂未支持回环检测、平面检测、遮挡检测、光照估计、环境理解等为实现更好的虚实叠加效果需具备的能力。未来会逐步实现优化并放开。


## API参考
### ArServiceSession类
该类负责管理和实现ArService的binder通信，并提供数据和控制接口。
| 方法名   |      描述      |
|----------|:-------------|
| ArServiceSession（Context context）   |      ArServiceSession类的构造方法，传入Application context，初始化相关内容      |
| getVersion()   |      获取ArService版本号      |
| create()   |      创建ArServiceSession，启动服务      |
| destroy()   |      销毁ArServiceSession，清理所有数据      |
| getPose()   |      获取当前设备相对于世界坐标系的位置和姿态      |
| getProjectionMatrix（int offset, float near, float far）   |      获取投影矩阵，可以用于在光机画面中渲染虚拟画面      |
| getViewMatrix（）   |      获取视图矩阵，可以用于在光机画面中渲染虚拟画面      |
| pause()   |      暂停，释放相机，但不清除数据|
| resume()   |      恢复运行      |



### 详细接口

#### ArServiceSession
```java
/** 创建与context绑定的session，用于启动和绑定service
 * @return void
 */
public ArServiceSession(Context context)

/**
 * 获取ArService版本号
 * 
 * @return 
 */
public String getVersion() 

/**
 * 创建Session，启动服务
 *
 * @return true:成功 false:失败
 */
public boolean create()

/**
 * 销毁Session，清理所有数据。但服务不一定会停止。如果还有其他进程正在访问服务，服务将继续提供。否则将停止服务。
 * @param
 * @return void
 */
public void destroy()

/**
 * 获取当前设备相对于世界坐标系的位置和姿态，包含一个位置向量和一个旋转四元数，
 * 数据存放顺序为：
 * [0]~[2]-位置向量：x，y，z
 * [3]~[6]-四元数：w，x，y，z
 * @return float[]
 */
public float[] getPose()

/**
 * 获取投影矩阵，可以用于在光机画面中渲染虚拟画面。
 * @param width:视口宽度，单位像素
 * @param height:视口高度，单位像素
 * @param near:近裁剪面距离，以米为单位
 * @param far:远裁剪面距离，以米为单位
 * @return float[] 16个浮点数表示的矩阵
 */
public float [] getProjectionMatrix(int width,int height,float near, float far)

/**
 * 获取光机的视图矩阵，可以用于在光机画面中渲染虚拟画面。
 * @return float[] 16个浮点数表示的矩阵
 */
public float [] getViewMatrix()

/**
 * 暂停，释放相机，但不清除数据。
 * @return void
 */
public void pause()

/**
 * 恢复运行。
 * @return void
 */
public void resume()
```


# Unity SDK
## SDK功能简介
本版块提供了INMO AIR2 SDK Unity平台的开发指南，其中介绍了如何在INMO Air2上的INMO RING2的适配开发，INMO Air2 AR眼镜触控板交互开发，以及Unity平台的AR能力开发套件。
## 快速上手
### 下载SDK
### 环境配置
#### 软件环境
- Unity Hub以及Unity 2020.3LTS或更高，并安装Android Support模块
- Visual Studio 2019
安装Unity Hub及Unity编辑器请访问Unity官方网站。https://unity.cn/
怎样安装Android Support？
打开Unity Hub，点击左方的安装，选中需要安装Android Support的版本，点击小齿轮按钮。

![](/images/install_unity.png)
点击添加模块，找到Android Build Support点击安装，安装完成后该版本下会显示Android标签。
Visual Studio 2019推荐使用上方的添加模块来安装。
Android NDK与Unity版本的对应关系如下，推荐使用Unity Hub进行安装
| Unity version | NDK version   |
|---------------|---------------|
| 2020.3 LTS    | r19           |
| 2021.3 LTS    | r21d          |

#### 硬件环境
- INMO AIR2 眼镜
- INMO RING2 指环
#### 导入SDK
inmo_unity_sdk包体结构如下：
![](/images/import_unity.png)
Samples是SDK提供的示例，SDK下是AR服务的aar库，URP是管线配置，如果你的项目已有URP配置或者使用Build-in管线可以删除URP文件夹，Document是离线文档，InmoUnitySdk是sdk程序集。
导入指南：
- 新建项目/现有项目，File->Build Settings切换到Android平台
![](/images/unity_build_android.png)
- 导入sdk提供的最新unitypackage：
  1. 直接将unitypackage拖入项目中Project窗口
  2. 通过Assets-->Import Package -> Custom Package
出现该页面点击右下角import按钮等待编译完成即可。
#### 打包设置
因为AR服务基于android-28开发，因此在打包时需要配置Minimum API Level为28
默认Scripting Backend是Mono模式，推荐使用Il2Cpp模式，具有更好的性能和扩展性。
SDK使用ARMv7构架，因此Target Architectures可以取消勾选ARM64来减少包体大小。
建议打包时把 “优化帧率”选项前的勾取消掉,如下图：
![](/images/unity_build_setting1.png)
![](/images/unity_build_setting2.png)

#### 3D模型资源要求
INMO AIR2 Unity开发推荐渲染设置
- 经测试同屏渲染约为10至20万面，模型需要轻量化，尽可能减少面数
- 仅使用一个Directional Light作为动态光源，多盏灯光使用光照贴图
- 设置Camera的Render Shadows关闭，Directional Light的Shadow Type为No Shadows
- Camera的Field Of View设置为13.4，Near设置为0.1，Far设置为15
- 避免使用低性能shader，如模型整体HighLight描边，会导致模型渲染面数翻倍

## INMO RING2开发适配
### 简介
本版块介绍如何使运行在INMO Air2 AR眼镜上的App适配INMO Ring2的交互
### 按键适配
| 按键             | 功能    | Keycode | 备注                 |
|----------------|-------|---------|--------------------|
| 单击/双击/长按 “OK”键 | 确认    | 13      | KeyCode.Return     |
| 单击/双击/长按 “上”键  | 方向上   | 273     | KeyCode.UpArrow    |
| 单击/双击/长按 “下”键  | 方向下   | 274     | KeyCode.DownArrow  |
| 单击/双击/长按 “左”键  | 方向左   | 276     | KeyCode.LeftArrow  |
| 单击/双击/长按 “右”键  | 方向右   | 275     | KeyCode.RightArrow |
| 单击“返回”键        | 退出/返回 | 27      | KeyCode.Escape     |

### 3dof射线
敬请期待
### 模式切换
指环共有三种控制模式：
长按指环返回键进行模式切换
#### 遥控器模式（默认模式）
- 只在按下按键时上报键值，不按键无任何事件。
#### 鼠标模式 
- 此模式下将戒指配置为一个标准的HID鼠标，通过传感器的变化控制鼠标x、y轴移动。系统底层会处理鼠标事件，应用一般情况下无需额外处理。
- 此模式下按键能正常工作，唯一不同的是“OK”键不再上报KeyCode.Return，而是作为鼠标的左键处理。
#### 射线模式
- 此模式下戒指将上报戒指自身的姿态数据，以四元数的方式上报，在unity中可以控制虚拟射线的指向，或者控制其他虚拟物体的旋转。
- 此模式下按键正常工作。


## INMO Air2 触控板交互适配
### 功能简介
本版块介绍如何在INMO Air2 AR眼镜上使用镜腿处的触控板构建开发运行在INMO Air2 AR眼镜上的Unity应用，参考SDK中的demo程序，可构建在INMO Air2 AR眼镜运行的基于触控交互的App；Demo将会提供一些关于如何构建在INMO Air2  AR眼镜运行并交互的控件使用方法，开发者可参考并构建属于自己的App。
### API参考
```csharp
private void Update()
{
    //监听回车键，映射单击右触摸板（确认按钮）
    if (Input.GetKeyDown(KeyCode.Return))
    {
        _singleClickToggle.isOn = true;
    }

    //监听ESC键，映射双击右触摸板（返回按钮）
    if (Input.GetKeyDown(KeyCode.Escape))
    {
        _doubleClickToggle.isOn = true;
    }

    //监听箭头键，映射单指在右触摸板上滑动
    if (Input.GetKeyDown(KeyCode.UpArrow))
    {
        _upScrollToggle.isOn = true;
    }
    if (Input.GetKeyDown(KeyCode.DownArrow))
    {
        _downScrollToggle.isOn = true;
    }
    if (Input.GetKeyDown(KeyCode.LeftArrow))
    {
        _forwardScrollToggle.isOn = true;
    }
    if (Input.GetKeyDown(KeyCode.RightArrow))
    {
        _backScrollToggle.isOn = true;
    }
}
```
### 键值表
| 手势/键值(右触控板)   | 功能    | Keycode | 备注                 |
|---------------|-------|---------|--------------------|
| 单指/双指 单击      | 确认    | 13      | KeyCode.Return     |
| 单指上划          | 方向上   | 273     | KeyCode.UpArrow    |
| 单指下划          | 方向下   | 274     | KeyCode.DownArrow  |
| 单指后划          | 方向左   | 276     | KeyCode.LeftArrow  |
| 单击/双击/长按 “右”键 | 方向右   | 275     | KeyCode.RightArrow |
| 单指/双指 双击      | 退出/返回 | 27      | KeyCode.Escape     |


## Unity AR开发
### 简介
本版块介绍了如何使用INMO AR服务构建可运行在INMO Air2 AR眼镜上的应用，INMO AR服务是一套用于在Inmo AR眼镜上构建增强现实应用的算法和服务集合。目前算法支持6dof空间定位能力，由于算力和电池容量的限制，暂未支持回环检测、平面检测、遮挡检测、光照估计、环境理解等为实现更好的虚实叠加效果需具备的能力。未来会逐步实现优化并放开。
### API参考
ARService类是AR功能的接口。
| 方法名                                | 描述|
|---------------------|------------------|
| ARService（Camera camera,float fov） | ARService类的构造方法，传入使用的camera和fov值，fov值建议设置为13.4          |
| getVersion()                       | 获取AR服务版本号                                               |
| create()                           | 创建并启动AR服务                                               |
| destroy()                          | 销毁AR服务，清理所有数据。但服务不一定会停止。如果还有其他进程正在访问服务，服务将继续提供。否则将停止服务。 |
| getPose()                          | 获取当前设备相对于世界坐标系的位置和姿态                                    |
| pause()                            | 暂停，释放相机，但不清除数据。                                         |
| resume()                           | 恢复运行。                                                   |

```csharp
namespace inmo.unity.sdk
{
    public class ARCamera : MonoBehaviour
    {
        //ar服务
        private ARService _arService;

        //眼镜在空间中的pose
        private ARPose _arpose;

        //air2 眼镜的 fov,推荐设置为13.9f
        [SerializeField] private float _fieldOfView = 13.4f;

        //ar状态的文本
        [SerializeField] private Text _arstatus;

        private void OnEnable()
        {
            //默认选中 InitARService 按钮
            GameObject.Find("UI Canvas/Buttons/InitAR").GetComponent<Button>().Select();
            _arstatus.text = "ar：未初始化";
        }

        private void Update()
        {
            //获取ar的arpose
            if(_arService != null && _arService.isOpen)
            {
                _arpose = _arService.GetARPose();
            }

            //双击右触摸板退出app
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit(0);
            }
        }

        private void LateUpdate()
        {
            //通过ar提供的arpose修改camera
            if (_arpose != null)
            {
                Camera.main.transform.position = _arpose.position;
                Camera.main.transform.rotation = _arpose.rotation;
            }
        }

        #region AR通信事件
        public void InitARService()
        {
            _arService = new ARService(Camera.main, _fieldOfView);
            _arstatus.text = "ar：已初始化";
        }

        public void CreateARService()
        {
            if (_arService != null)
            {
                _arService.Create();
                _arstatus.text = "ar：已开启";
            }
        }

        public void PauseARService()
        {
            if (_arService != null)
            {
                _arService.Pause();
                _arstatus.text = "ar：已暂停";
            }
        }

        public void ResumeARService()
        {
            if (_arService != null)
            {
                _arService.Resume();
                _arstatus.text = "ar：已开启";
            }
        }

        public void DestroyARService()
        {
            if (_arService != null)
            {
                _arService.Destroy();
                _arstatus.text = "ar：已销毁";
            }
        }

        public void GetARServiceVersion()
        {
            if (_arService != null)
            {
                string version = _arService.GetVersion();
                _arstatus.text = "ar：" + version;
            }
        }
        #endregion
    }
}

```
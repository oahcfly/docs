# Air2 开启adb
## 1、开启系统后进入设置页面，找到《我的眼镜》
![](/images/my_glass.png)
## 2、长按两次镜腿，触发adb风险提示页面
![](/images/adb_warning.png)
## 3、再次长按镜腿，进入密码输入，输入正确密码后即可开启adb调试

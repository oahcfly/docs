# 影目开发者条款
## 总则
为维护系统安全、保障用户合法权益与个人信息安全，在影目设备上安装运行的应用应符合本条款要求。随着法律法规、行业规范、当代社会价值观的动态更新，影目也将定期对本条款进行更新。

## 技术
### 1、兼容稳定
在影目设备上运行的应用需适配影目Air机型，不得出现以下情况：
1、应用频繁出现崩溃。
2、应用无法正常安装、启动、卸载。
3、功能未实现或点击后无响应或程序本身存在错误。
4、应用显示的图像、文本和其他界面元素有明显的失真、模糊或像素化。

### 2、安全
在影目设备上运行的应用必须通过影目的安全要求，不得出现以下安全不达标的情况：
1、应用含有病毒木马等侵害用户的功能（包括代码等可疑行为），并限制下载或安装。
2、应用不得下载、安装或执行会引入或更改应用特性或功能的代码，也不得含有第三方加载可执行代码的应用或SDK，也不得下载、安装或执行会引入或更改APP特性或功能的代码，包括其他APP。
3、除支持核心功能而需要的最低级别权限外，应用擅自申请其他无关的权限。
4、应用签名、应用文件与影目应用市场检测过的同包名应用的签名与应用文件不一致。
5、不得恶意攻击/破坏系统或设备运行，导致系统崩溃、系统重启、无响应、黑屏等，或在用户不知情或未授权的情况下，对用户文件、系统文件或其它非恶意软件进行病毒感染、劫持、篡改、删除、卸载、或限制运行等。
6、不得利用系统已知安全漏洞、或私自调取未发布的系统能力，用于实现商业目的或导致用户信息安全和使用体验受到影响。

### 3、性能
在影目设备上运行的应用必须通过影目的性能要求，不得出现性能不达标的情况，验收标准如下：
![](/images/software_acceptance_criteria.PNG)


4、功耗
在影目设备上运行的应用必须通过在影目的功耗要求，不得出现以下功耗不达标的情况：
1、应用转入后台时，有服务处于运行状态（该服务与应用核心功能相关且必要的除外）。
2、应用转入后台时，未主动释放占用资源。
3、应用转入后台后，私自启动。
4、应用转入后台时，有持锁行为。
5、应用转入后台时，占用系统资源（例如，非业务需要占用蓝牙以及GPS系统资源等）。
验收标准如下：
![](/images/hardware_acceptance_criteria.PNG)
## 合规
### 1、隐私
为保障用户权益与个人信息安全，依据《中华人民共和国个人信息保护法》、《中华人民共和国网络安全法》、《App违法违规收集使用个人信息行为认定方法》、《关于开展APP侵害用户权益专项整治工作的通知》、《工业和信息化部关于开展纵深推进APP侵害用户权益专项整治行动的通知》、《信息安全技术 个人信息安全规范》的相关规定和政策，开发者提交的应用需遵守上述规定。
### 2、知识产权
开发者须确认提供的应用为原创或获得合法授权，不得在未经授权的情况下，使用他人的成果。应用应符合以下要求，如存在侵犯他人知识产权的情况，影目将有权拒绝应用发布和更新申请。
1、开发者不得未经授权使用其他公司的商标。
2、开发者需获取许可授权，才能在应用内使用他人知识产权成果和内容。
3、开发者使用开源代码需遵守开源许可协议，不得将开源代码视为自有代码使用。
### 3、内容合规
1、应用内容不得违反国家法规政策。
1.1 应用内不得含有违反宪法确定的基本原则的内容。
1.2 应用内不得含有危害国家统一、主权和领土完整的内容。
1.3 应用内不得含有泄露国家秘密、危害国家安全或者损害国家荣誉和利益的内容。
1.4 应用内不得含有散布谣言，扰乱社会秩序，破坏社会稳定的内容。
1.5 应用内不得含有淫秽、色情、低俗、非法交易、赌博、暴力，或者教唆犯罪的内容。
1.6 应用内不得含有违背社会公德的内容。
1.7 应用内不得含有煽动民族仇恨、民族或种族歧视，破坏民族团结，或者侵害民族风俗、习惯的内容。
1.8 应用内不得含诽谤、人身攻击或者侮辱性的内容。
1.9 应用内不得包含具有贩卖、购买违禁物品的内容。
1.10 儿童类应用内容需适合儿童，不得含有其他会对儿童造成干扰的内容，儿童类应用不得含有外链接、购买机会，除非其保留在受家长监控的指定区域中。
1.11应用不得含有宣扬邪教和封建迷信的内容，也不得含有关于宗教、种族、性取向、性别或其他目标群体的诽谤或恶意内容。
1.12 应用内不得过度宣传酒精和危险物品（如毒药、爆炸物等），或鼓励未成年人消费香烟和酒精饮料。
1.13 应用内不得含有法律、行政法规和国家规定禁止的其他内容。


### 4、用户体验
- 为保障用户权益，和用户体验，不允许应用未经用户许可，进行下载和安装或访问其他应用。
- 应用不得出现无法正常安装、安装时提示解析失败、启动、卸载、运行崩溃或出现需借助第三方软件才可卸载的情况。
- 不得在应用外的任何其他页面弹出图文、视频广告，也不得在系统通知中推送广告内容。
- 不得频繁推系统通知。
- 不得申请屏幕常亮权限，以免导致屏幕烧屏。
- 不得含有隐藏或不被用户感知或发现的功能，或者留有后门程序。